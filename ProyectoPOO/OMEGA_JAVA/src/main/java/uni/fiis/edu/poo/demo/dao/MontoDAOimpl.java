package uni.fiis.edu.poo.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uni.fiis.edu.poo.demo.bean.Monto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MontoDAOimpl implements MontoDAO {

    @Autowired
    private JdbcTemplate template;

    @Override
    public void registrarMonto(Monto monto) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "INSERT INTO MONTOS VALUES(?, ?);";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(1, monto.getCant_persona());
        pst.setDouble(2, monto.getMonto());
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void actualizarMonto(byte cant_persona, double monto) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "UPDATE MONTOS SET MONTO = ? WHERE CANT_PERSONA = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setDouble(1, monto);
        pst.setByte(2,cant_persona);
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void eliminarMonto(byte cant_persona) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "DELETE FROM MONTOS WHERE CANT_PERSONA = ?";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(1, cant_persona);
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public Monto buscarMonto(byte cant_persona) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM MONTOS WHERE CANT_PERSONA = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(1, cant_persona);
        ResultSet rs = pst.executeQuery();
        Monto mont = null;
        if (rs.next()){
            double monto = rs.getDouble(2);
            mont = new Monto(cant_persona,monto);
        }
        rs.close();
        pst.close();
        connection.close();
        return mont;
    }

    @Override
    public List<Monto> mostrarMonto() throws Exception {
        Connection connection = template.getDataSource().getConnection();
        List<Monto> montos = new ArrayList<>();
        Statement st = connection.createStatement();
        String sql = "SELECT * FROM MONTOS ORDER BY MONTO;";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()){
            Byte cant_persona = rs.getByte(1);
            double monto = rs.getDouble(2);
            Monto mont = new Monto(cant_persona,monto);
            montos.add(mont);
        }
        rs.close();
        st.close();
        connection.close();
        return montos;
    }
}
