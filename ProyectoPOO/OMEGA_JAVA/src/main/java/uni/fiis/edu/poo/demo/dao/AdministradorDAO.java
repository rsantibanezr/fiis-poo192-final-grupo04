package uni.fiis.edu.poo.demo.dao;

import uni.fiis.edu.poo.demo.bean.Administrador;

import java.sql.Connection;
import java.util.List;

public interface AdministradorDAO {
    public void registrarAdministrador(Administrador administrador) throws Exception;
    public void actualizarAdministrador(String email, String newpassword) throws Exception;
    public Administrador buscarAdministrador(String email) throws Exception;
    public void eliminarAdministrador(String email) throws Exception;
    public List<Administrador> mostrarAdministrador() throws Exception;
    public int cantidadAdministrador() throws Exception;
    public void darPrivilegio(String email) throws Exception;
    public void quitarPrivilegio(String email) throws Exception;
    public void disminuirID(int id_admin) throws Exception;
}
