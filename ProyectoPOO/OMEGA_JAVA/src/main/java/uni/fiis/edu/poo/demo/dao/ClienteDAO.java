package uni.fiis.edu.poo.demo.dao;

import uni.fiis.edu.poo.demo.bean.Cliente;

import java.sql.Connection;
import java.util.List;

public interface ClienteDAO {
    public void registrarCliente(Cliente cliente) throws Exception;
    public void actualizarCliente(String cod_uni, String newpassord) throws Exception;
    public void eliminarCliente(String cod_uni) throws Exception;
    public Cliente buscarCliente(String cod_uni) throws Exception;
    public List<Cliente> mostrarCliente() throws Exception;
}
