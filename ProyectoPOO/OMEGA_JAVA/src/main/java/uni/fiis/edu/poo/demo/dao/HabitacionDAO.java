package uni.fiis.edu.poo.demo.dao;

import uni.fiis.edu.poo.demo.bean.Habitacion;

import java.sql.Connection;
import java.util.List;

public interface HabitacionDAO {
    public void registrarHabitacion(Habitacion habitacion) throws Exception;
    public void actualizarHabitacion(byte id_hab, String descripcion) throws Exception;
    public void eliminarHabitacion(byte id_hab) throws Exception;
    public Habitacion buscarHabitacion(byte id_hab) throws Exception;
    public List<Habitacion> mostrarHabitacion() throws Exception;
    public byte cantidadHabitacion() throws Exception;
    public void disminuirID(byte id_hab) throws Exception;
}
