package uni.fiis.edu.poo.demo.request;

public class MontoRequest {
    private byte cant_persona;
    private double monto;

    public byte getCant_persona() {
        return cant_persona;
    }

    public void setCant_persona(byte cant_persona) {
        this.cant_persona = cant_persona;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
}
