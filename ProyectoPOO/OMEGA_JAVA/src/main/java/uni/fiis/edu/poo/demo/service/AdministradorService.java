package uni.fiis.edu.poo.demo.service;

import org.apache.naming.factory.SendMailFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uni.fiis.edu.poo.demo.bean.Administrador;
import uni.fiis.edu.poo.demo.dao.AdministradorDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

@Service
public class AdministradorService {

    private boolean faltagrave=true;

    public boolean isFaltagrave() {
        return faltagrave;
    }

    public void setFaltagrave(boolean faltagrave) {
        this.faltagrave = faltagrave;
    }

    @Autowired
    private JdbcTemplate template;

    @Autowired
    AdministradorDAO administradorDAO;

    public ResponseEntity registrarAdministrador(Administrador administrador) throws Exception{
        Connection connection = template.getDataSource().getConnection();
        String sql1 = "SELECT COUNT(*) FROM ADMINISTRADOR WHERE EMAIL = ?;";
        PreparedStatement pst1 = connection.prepareStatement(sql1);
        pst1.setString(1,administrador.getEmail());
        ResultSet rs = pst1.executeQuery();
        rs.next();
        int cant = rs.getInt(1);
        rs.close();
        pst1.close();
        connection.close();
        if(cant == 0){
            administradorDAO.registrarAdministrador(administrador);
            return new ResponseEntity("Administrador registrado.", HttpStatus.OK);
        }else {
            return new ResponseEntity("Email ya asociado.",HttpStatus.OK);
        }
    }

    public void actualizarAdministrador(String email, String newpassord) throws Exception{
        administradorDAO.actualizarAdministrador(email,newpassord);
    }

    public Administrador buscarAdministrador(String email) throws Exception{
        return administradorDAO.buscarAdministrador(email);
    }

    public void eliminarAdministrador(String email) throws Exception{
        administradorDAO.eliminarAdministrador(email);
    }

    public List<Administrador> mostrarAdministrador() throws Exception{
        return administradorDAO.mostrarAdministrador();
    }

    public int cantidadAdministrador() throws Exception{
        return administradorDAO.cantidadAdministrador();
    }

    public void darPrivilegio(String email) throws Exception{
        administradorDAO.darPrivilegio(email);
    }

    public void quitarPrivilegio(String email) throws Exception{
        administradorDAO.quitarPrivilegio(email);
    }

    public void disminuirID(int id_admin) throws Exception{
        administradorDAO.disminuirID(id_admin);
    }
}
