package uni.fiis.edu.poo.demo.bean;

public class Cliente extends Usuario {
    private String cod_uni; //PK
    private byte ciclo;
    private String facultad;

    public Cliente(String cod_uni, String password, String nombre, String ap_paterno, String ap_materno, String dni, byte ciclo, String facultad, String email, int telefono) {
        this.cod_uni = cod_uni;
        this.password = password;
        this.nombre = nombre;
        this.ap_paterno = ap_paterno;
        this.ap_materno = ap_materno;
        this.dni = dni;
        this.ciclo = ciclo;
        this.facultad = facultad;
        this.email = email;
        this.telefono = telefono;
    }

    public String getCod_uni() {return cod_uni;}

    public String getPassword(){return password;}

    public String getNombre(){
        return nombre;
    }

    public String getAp_paterno(){
        return ap_paterno;
    }

    public String getAp_materno(){
        return ap_materno;
    }

    public String getDni(){
        return dni;
    }

    public byte getCiclo() {return ciclo;}

    public String getFacultad() {return facultad;}

    public String getEmail(){
        return email;
    }

    public int getTelefono(){
        return telefono;
    }
}
