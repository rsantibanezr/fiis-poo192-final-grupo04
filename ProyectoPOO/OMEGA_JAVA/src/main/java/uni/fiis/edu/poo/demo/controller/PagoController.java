package uni.fiis.edu.poo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uni.fiis.edu.poo.demo.bean.Pago;
import uni.fiis.edu.poo.demo.dao.PagoDAO;
import uni.fiis.edu.poo.demo.dao.PagoDAOimpl;
import uni.fiis.edu.poo.demo.service.PagoService;

@RestController
public class PagoController {

    @Autowired
    PagoService pagoService;

    @GetMapping("/pago/registrar")
    public ResponseEntity registrarPago(@RequestParam String credit_card,@RequestParam byte cant_persona, @RequestParam String fecha) throws Exception{
        int id_pago = pagoService.cantidadPago();
        id_pago ++;
        Pago pago = new Pago(id_pago,credit_card,cant_persona,fecha);
        try {
            pagoService.registrarPago(pago);
            return new ResponseEntity("Pago registrado.", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("Pago no registrada.",HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("pago/eliminar")
    public ResponseEntity eliminarPago(@RequestParam int id_pago) throws Exception{
        try {
            if (pagoService.buscarPago(id_pago)!=null){
                pagoService.eliminarPago(id_pago);
                return new ResponseEntity("Pago " + id_pago + " eliminado.",HttpStatus.OK);
            }else{
                return new ResponseEntity("Pago no existe.", HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity("PAGO imposible de eliminar", HttpStatus.BAD_GATEWAY);
        }
    }

    @PostMapping("/pago/actualizar")
    public ResponseEntity actualizarPago(@RequestParam int id_pago, @RequestParam byte cant_persona) throws Exception{
        try {
            pagoService.actualizarPago(id_pago,cant_persona);
            return new ResponseEntity("Monto de "+String.valueOf((int)cant_persona)+" personas, actualizado",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("Monto no se pudo actualizar.",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/pago/buscar")
    public ResponseEntity buscarPago(@RequestParam int id_pago) throws Exception{
        try {
            return new ResponseEntity(pagoService.buscarPago(id_pago),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar.", HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/pago/mostrar")
    public ResponseEntity mostrarPago() throws Exception{
        try {
            return new ResponseEntity(pagoService.mostrarPago(),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar montos.",HttpStatus.BAD_GATEWAY);
        }
    }
}
