package uni.fiis.edu.poo.demo.dao;

import uni.fiis.edu.poo.demo.bean.Monto;

import java.sql.Connection;
import java.util.List;

public interface MontoDAO {
    public void registrarMonto(Monto monto) throws Exception;
    public void actualizarMonto(byte cant_persona, double monto) throws Exception;
    public void eliminarMonto(byte cant_persona) throws Exception;
    public Monto buscarMonto(byte cant_persona) throws Exception;
    public List<Monto> mostrarMonto() throws Exception;
}
