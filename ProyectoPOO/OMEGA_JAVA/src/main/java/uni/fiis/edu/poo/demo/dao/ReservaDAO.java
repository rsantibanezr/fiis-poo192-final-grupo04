package uni.fiis.edu.poo.demo.dao;

import uni.fiis.edu.poo.demo.bean.Reserva;

import java.sql.Connection;
import java.util.List;

public interface ReservaDAO {
    public void registrarReserva(Reserva reserva) throws Exception;
    public void actualizarReserva(String cod_uni, String fecha) throws Exception;
    public void eliminarReserva(String cod_uni, String fecha) throws Exception;
    public Reserva buscarReserva(String cod_uni) throws Exception;
    public List<Reserva> mostrarReserva() throws Exception;
}
