package uni.fiis.edu.poo.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uni.fiis.edu.poo.demo.bean.Cliente;
import uni.fiis.edu.poo.demo.bean.Habitacion;
import uni.fiis.edu.poo.demo.dao.ClienteDAOimpl;
import uni.fiis.edu.poo.demo.dao.HabitacionDAO;
import uni.fiis.edu.poo.demo.dao.HabitacionDAOimpl;
import uni.fiis.edu.poo.demo.service.HabitacionService;

import java.util.List;

@RestController
public class HabitacionController {

    @Autowired
    HabitacionService habitacionService;

    @PostMapping("/habitacion/registrar")
    public ResponseEntity registrarHabitacion(@RequestParam String descripcion) throws Exception {
        byte id_hab = habitacionService.cantidadHabitacion();
        id_hab ++;
        Habitacion habitacion = new Habitacion(id_hab, descripcion);
        try {
            habitacionService.registrarHabitacion(habitacion);
            return new ResponseEntity("Habitacion registrada.", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("Habitacion no registrada.",HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/habitacion/buscar")
    public ResponseEntity buscarHabitacion(@RequestParam byte id_hab) throws Exception {
        try{
            if(habitacionService.buscarHabitacion(id_hab) != null){
                Habitacion habitacion = habitacionService.buscarHabitacion(id_hab);
                return new ResponseEntity("Habitacion encontrada.",HttpStatus.OK);
            }else{
                return new ResponseEntity("Habitacion NO encontrada.",HttpStatus.OK);
            }
        }
        catch (Exception e){
            return new ResponseEntity("Habitacion imposible de encontrar.",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/habitacion/actualizar")
    public ResponseEntity actualizarHabitacion(@RequestParam byte id_hab, @RequestParam String newdescripcion) throws Exception{
        try {
            if (habitacionService.buscarHabitacion(id_hab) != null) {
                habitacionService.actualizarHabitacion(id_hab, newdescripcion);
                return new ResponseEntity("Habitacion: "+ id_hab +" ha sido actualizado.",HttpStatus.OK);
            }else{
                return new ResponseEntity("Usuario no existe.",HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity("No se puedo actualizar usuario.",HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/habitacion/eliminar")
    public ResponseEntity eliminarHabitacion(@RequestParam byte id_hab) throws Exception{
        try {
            if (habitacionService.buscarHabitacion(id_hab) != null) {
                habitacionService.eliminarHabitacion(id_hab);
                return new ResponseEntity("Habitacion " + id_hab + " eliminada.",HttpStatus.OK);
            }else{
                return new ResponseEntity("Habitacion no existe.", HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity("Habitacion imposible de eliminar", HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/habitacion/mostrar")
    public ResponseEntity mostrarHabitacion() throws Exception{
        try {
            return new ResponseEntity(habitacionService.mostrarHabitacion(),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar habitaciones.",HttpStatus.BAD_GATEWAY);
        }

    }


}
