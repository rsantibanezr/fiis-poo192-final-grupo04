package uni.fiis.edu.poo.demo.dao;

import uni.fiis.edu.poo.demo.bean.Pago;
import java.sql.Connection;
import java.util.List;

public interface PagoDAO {
    public void registrarPago(Pago pago) throws Exception;
    public void actualizarPago(int id_pago, byte cant_persona) throws Exception;
    public void eliminarPago(int id_pago) throws Exception;
    public Pago buscarPago(int id_pago) throws Exception;
    public List<Pago> mostrarPago() throws Exception;
    public int cantidadPago() throws Exception;
    public void disminuirID(int id_pago) throws Exception;
}
