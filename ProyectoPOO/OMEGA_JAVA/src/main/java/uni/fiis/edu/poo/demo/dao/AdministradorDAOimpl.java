package uni.fiis.edu.poo.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uni.fiis.edu.poo.demo.bean.Administrador;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AdministradorDAOimpl implements AdministradorDAO {

    @Autowired
    private JdbcTemplate template;

    @Override
    public void registrarAdministrador(Administrador administrador) throws Exception {
        Connection connection = template.getDataSource().getConnection();

        String sql = "INSERT INTO ADMINISTRADOR VALUES(?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setInt(1, administrador.getId_admin());
        pst.setString(2, administrador.getPassword());
        pst.setString(3, administrador.getNombre());
        pst.setString(4,administrador.getAp_paterno());
        pst.setString(5,administrador.getAp_materno());
        pst.setString(6,administrador.getDni());
        pst.setString(7,administrador.getEmail());
        pst.setInt(8,administrador.getTelefono());
        pst.execute();
        /*Statement st = connection.createStatement();
        String sql2="CREATE USER " + administrador.getEmail() + "PASSWORD '" + administrador.getPassword() + "';";
        st.execute(sql2);
        st.close();
        darPrivilegio(administrador.getEmail());*/
        pst.close();
        connection.close();
    }

    @Override
    public void actualizarAdministrador(String email, String newpassword) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "UPDATE ADMINISTRADOR SET CONTRASEÑA = ? WHERE EMAIL = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1,newpassword);
        pst.setString(2,email);
        pst.execute();
        pst.close();
        sql = "ALTER USER " + email + " with password ‘" + newpassword + "’;";
        Statement st = connection.createStatement();
        st.execute(sql);
        st.close();
        connection.close();
    }

    @Override
    public Administrador buscarAdministrador(String email)throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM ADMINISTRADOR WHERE EMAIL = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, email);
        ResultSet rs = pst.executeQuery();
        Administrador admin = null;
        if (rs.next()){
            int id_admin = rs.getInt(1);
            String password = rs.getString(2);
            String nombre = rs.getString(3);
            String ap_paterno = rs.getString(4);
            String ap_materno = rs.getString(5);
            String dni = rs.getString(6);
            int telefono = rs.getInt(8);
            admin = new Administrador(id_admin, password,nombre,ap_paterno,ap_materno,dni,email,telefono);
        }
        rs.close();
        pst.close();
        connection.close();
        return admin;
    }

    @Override
    public void eliminarAdministrador(String email)throws Exception {
        Connection connection = template.getDataSource().getConnection();
        //quitarPrivilegio(email);
        String sql = "DELETE FROM ADMINISTRADOR WHERE EMAIL = ?";
        PreparedStatement pst1 = connection.prepareStatement(sql);
        pst1.setString(1, email);
        pst1.execute();
        pst1.close();
        /*sql = "DROP USER ?;";
        PreparedStatement pst2 = connection.prepareStatement(sql);
        pst2.setString(1,email);
        pst2.execute();
        pst2.close();*/
        connection.close();
    }

    @Override
    public List<Administrador> mostrarAdministrador() throws Exception {
        Connection connection = template.getDataSource().getConnection();
        List<Administrador> administrador = new ArrayList<>();
        Statement st = connection.createStatement();
        String sql = "SELECT * FROM ADMINISTRADOR ORDER BY ID_ADMIN;";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()){
            int id_admin = rs.getInt(1);
            String password = rs.getString(2);
            String nombre = rs.getString(3);
            String ap_paterno = rs.getString(4);
            String ap_materno = rs.getString(5);
            String dni = rs.getString(6);
            String email = rs.getString(7);
            int telefono = rs.getInt(8);
            Administrador admin = new Administrador(id_admin, password,nombre,ap_paterno,ap_materno,dni,email,telefono);
            administrador.add(admin);
        }
        rs.close();
        st.close();
        connection.close();
        return administrador;
    }

    @Override
    public int cantidadAdministrador() throws Exception{
        Connection connection = template.getDataSource().getConnection();
        Statement st = connection.createStatement();
        String sql = "SELECT COUNT (*) FROM ADMINISTRADOR;";
        ResultSet rs = st.executeQuery(sql);
        rs.next();
        int cant = rs.getInt(1);
        rs.close();
        st.close();
        connection.close();
        return cant;
    }

    @Override
    public void darPrivilegio(String email) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "GRANT ALL PRIVILEGES ON DATABASE d5u9b0bflptaam to "+ email + ";";
        Statement st = connection.createStatement();
        st.execute(sql);
        st.close();
        connection.close();
    }

    @Override
    public void quitarPrivilegio(String email) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "REVOKE ALL ON DATABASE d5u9b0bflptaam FROM " + email +";";
        Statement st = connection.createStatement();
        st.execute(sql);
        connection.close();
    }

    @Override
    public void disminuirID(int id_admin) throws Exception{
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM ADMINISTRADOR ORDER BY ID_ADMIN;";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            if(rs.getInt(1)>id_admin){
                String sql2 = "UPDATE ADMINISTRADOR SET ID_ADMIN = ID_ADMIN - 1" + "\nWHERE ID_ADMIN = ? ;";
                PreparedStatement pst = connection.prepareStatement(sql2);
                pst.setInt(1,rs.getInt(1));
                pst.executeUpdate();
                pst.close();
            }
        }
        rs.close();
        st.close();
        connection.close();
    }
}
