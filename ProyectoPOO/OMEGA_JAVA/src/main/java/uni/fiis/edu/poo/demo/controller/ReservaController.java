package uni.fiis.edu.poo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import uni.fiis.edu.poo.demo.bean.Reserva;
import uni.fiis.edu.poo.demo.dao.ReservaDAO;
import uni.fiis.edu.poo.demo.dao.ReservaDAOimpl;
import uni.fiis.edu.poo.demo.service.ReservaService;

import java.util.List;

@RestController
public class ReservaController {

    @Autowired
    ReservaService reservaService;

    @PostMapping("/reserva/registrar")
    public ResponseEntity registrarReserva(@RequestParam String cod_uni,@RequestParam int id_pago, @RequestParam byte id_hab, @RequestParam String fecha) throws Exception{
        try {
            return reservaService.registrarReserva(new Reserva(cod_uni, id_pago, id_hab, fecha));
        }catch (Exception e){
            return new ResponseEntity("Pago no registrada.",HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/reserva/actualizar")
    public ResponseEntity actualizarReserva(@RequestParam String cod_uni, @RequestParam String fecha) throws Exception{
        try {
            reservaService.actualizarReserva(cod_uni,fecha);
            return new ResponseEntity("Pago del usuario " + cod_uni + " actualizado.",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("Monto no se pudo actualizar.",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/reserva/eliminar")
    public ResponseEntity eliminarReserva(@RequestParam String cod_uni, @RequestParam String fecha) throws Exception{
        try {
            if (reservaService.buscarReserva(cod_uni)!=null){
                reservaService.eliminarReserva(cod_uni, fecha);
                return new ResponseEntity("Pago del usuario " + cod_uni + " con fecha "+ fecha +" eliminado.",HttpStatus.OK);
            }else{
                return new ResponseEntity("Pago no existe.", HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity("PAGO imposible de eliminar", HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/reserva/buscar")
    public ResponseEntity buscarReserva(@RequestParam String cod_uni) throws Exception{
        try {

            return new ResponseEntity(reservaService.buscarReserva(cod_uni),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar.", HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/reserva/mostrar")
    public ResponseEntity mostrarReserva() throws Exception{
        try {
            return new ResponseEntity(reservaService.mostrarReserva(),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar montos.",HttpStatus.BAD_GATEWAY);
        }
    }

}
