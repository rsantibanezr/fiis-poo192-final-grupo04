package uni.fiis.edu.poo.demo.request;

public class PagoRequest {
    private int id_pago;
    private String credit_card;
    private byte cant_persona;
    private String fecha;

    public int getId_pago() {
        return id_pago;
    }

    public void setId_pago(int id_pago) {
        this.id_pago = id_pago;
    }

    public String getCredit_card() {
        return credit_card;
    }

    public void setCredit_card(String credit_card) {
        this.credit_card = credit_card;
    }

    public byte getCant_persona() {
        return cant_persona;
    }

    public void setCant_persona(byte cant_persona) {
        this.cant_persona = cant_persona;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
