package uni.fiis.edu.poo.demo.bean;

public class Administrador extends Usuario {
    private int id_admin; //PK

    public Administrador(int id_admin, String password, String nombre, String ap_paterno, String ap_materno, String dni, String email, int telefono) {
        this.id_admin = id_admin;
        this.password = password;
        this.nombre = nombre;
        this.ap_paterno = ap_paterno;
        this.ap_materno = ap_materno;
        this.dni = dni;
        this.email = email;
        this.telefono = telefono;
    }

    public int getId_admin() {
        return id_admin;
    }

    public void setId_admin(int id_admin) {
        this.id_admin = id_admin;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getNombre(){
        return nombre;
    }

    public String getAp_paterno(){
        return ap_paterno;
    }

    public String getAp_materno(){
        return ap_materno;
    }

    public String getDni(){
        return dni;
    }

    public String getEmail(){
        return email;
    }

    public int getTelefono(){
        return telefono;
    }
}
