package uni.fiis.edu.poo.demo.bean;

public abstract class Usuario {
    protected String password;
    protected String nombre;
    protected String ap_paterno;
    protected String ap_materno;
    protected String dni;
    protected String email;
    protected int telefono;
}
