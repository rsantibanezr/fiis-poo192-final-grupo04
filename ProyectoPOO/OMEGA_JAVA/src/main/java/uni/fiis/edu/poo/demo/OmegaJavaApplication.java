package uni.fiis.edu.poo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OmegaJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OmegaJavaApplication.class, args);
	}

}
