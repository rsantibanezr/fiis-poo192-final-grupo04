package uni.fiis.edu.poo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uni.fiis.edu.poo.demo.bean.Monto;
import uni.fiis.edu.poo.demo.dao.MontoDAO;
import uni.fiis.edu.poo.demo.dao.MontoDAOimpl;
import uni.fiis.edu.poo.demo.service.MontoService;

@RestController
public class MontoController {

    @Autowired
    MontoService montoService;

    @PostMapping("/monto/registrar")
    public ResponseEntity registrarMonto(@RequestParam byte cant_persona, @RequestParam double monto) throws Exception{
        Monto monto1 = new Monto(cant_persona,monto);
        try {
            return montoService.registrarMonto(monto1);
        }catch (Exception e){
            return new ResponseEntity("Monto no se pudo registrar.",HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/monto/actualizar")
    public ResponseEntity actualizarMonto(@RequestParam byte cant_persona, @RequestParam double monto) throws Exception{
        try {
            montoService.actualizarMonto(cant_persona,monto);
            return new ResponseEntity("Monto de "+String.valueOf((int)monto)+" personas, actualizado",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("Monto no se pudo actualizar.",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/monto/eliminar")
    public ResponseEntity eliminarMonto(@RequestParam byte cant_persona) throws Exception{
        try{
            montoService.eliminarMonto(cant_persona);
            return new ResponseEntity("Monto eliminado.", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("Monto no se pudo eliminar.", HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/monto/buscar")
    public ResponseEntity buscarMonto(@RequestParam byte cant_persona) throws Exception{
        try {
            return new ResponseEntity(montoService.buscarMonto(cant_persona),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar.", HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/monto/mostrar")
    public ResponseEntity mostrarMonto() throws Exception{
        try {
            return new ResponseEntity(montoService.mostrarMonto(),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar montos.",HttpStatus.BAD_GATEWAY);
        }
    }
}
