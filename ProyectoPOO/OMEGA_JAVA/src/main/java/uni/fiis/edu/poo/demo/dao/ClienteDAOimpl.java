package uni.fiis.edu.poo.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uni.fiis.edu.poo.demo.bean.Cliente;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ClienteDAOimpl implements ClienteDAO {

    @Autowired
    private JdbcTemplate template;

    @Override
    public void registrarCliente(Cliente cliente) throws Exception {
        Connection connection = template.getDataSource().getConnection();

        String sql = "INSERT INTO CLIENTE VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, cliente.getCod_uni());
        pst.setString(2, cliente.getPassword());
        pst.setString(3, cliente.getNombre());
        pst.setString(4,cliente.getAp_paterno());
        pst.setString(5,cliente.getAp_materno());
        pst.setString(6,cliente.getDni());
        pst.setByte(7,cliente.getCiclo());
        pst.setString(8,cliente.getFacultad());
        pst.setString(9,cliente.getEmail());
        pst.setInt(10,cliente.getTelefono());
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void actualizarCliente(String cod_uni, String newpassord) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "UPDATE CLIENTE SET CONTRASEÑA = ? WHERE COD_UNI = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1,newpassord);
        pst.setString(2,cod_uni);
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void eliminarCliente(String cod_uni) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "DELETE FROM CLIENTE WHERE COD_UNI = ?";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, cod_uni);
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public Cliente buscarCliente(String cod_uni) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM CLIENTE WHERE COD_UNI = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, cod_uni);
        ResultSet rs = pst.executeQuery();
        Cliente cliente = null;
        if (rs.next()){
            String password = rs.getString(2);
            String nombre = rs.getString(3);
            String ap_paterno = rs.getString(4);
            String ap_materno = rs.getString(5);
            String dni = rs.getString(6);
            Byte ciclo = rs.getByte(7);
            String facultad = rs.getString(8);
            String email = rs.getString(9);
            int telefono = rs.getInt(10);
            cliente = new Cliente(cod_uni, password,nombre,ap_paterno,ap_materno,dni,ciclo,facultad,email,telefono);
        }
        rs.close();
        pst.close();
        connection.close();
        return cliente;
    }

    @Override
    public List<Cliente> mostrarCliente() throws Exception {
        Connection connection = template.getDataSource().getConnection();
        List<Cliente> cliente = new ArrayList<>();
        Statement st = connection.createStatement();
        String sql = "SELECT * FROM CLIENTE;";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()){
            String cod_uni = rs.getString(1);
            String password = rs.getString(2);
            String nombre = rs.getString(3);
            String ap_paterno = rs.getString(4);
            String ap_materno = rs.getString(5);
            String dni = rs.getString(6);
            Byte ciclo = rs.getByte(7);
            String facultad = rs.getString(8);
            String email = rs.getString(9);
            int telefono = rs.getInt(10);
            Cliente cl = new Cliente(cod_uni, password,nombre,ap_paterno,ap_materno,dni,ciclo,facultad,email,telefono);
            cliente.add(cl);
        }
        rs.close();
        st.close();
        connection.close();
        return cliente;
    }
}
