package uni.fiis.edu.poo.demo.bean;

public class Monto {
    private byte cant_persona; //PK
    private double monto;

    public Monto(byte cant_persona, double monto) {
        this.cant_persona = cant_persona;
        this.monto = monto;
    }

    public byte getCant_persona() {
        return cant_persona;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
}
