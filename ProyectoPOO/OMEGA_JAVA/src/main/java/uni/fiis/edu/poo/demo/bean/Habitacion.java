package uni.fiis.edu.poo.demo.bean;

public class Habitacion{
    private byte id_hab; //PK
    private String descripcion;

    public Habitacion(byte id_hab, String descripcion) {
        this.id_hab = id_hab;
        this.descripcion = descripcion;
    }

    public byte getId_hab() {
        return id_hab;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
