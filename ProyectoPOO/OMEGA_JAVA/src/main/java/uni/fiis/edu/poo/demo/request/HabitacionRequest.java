package uni.fiis.edu.poo.demo.request;

public class HabitacionRequest {
    private byte id_hab;
    private String descripcion;

    public byte getId_hab() {
        return id_hab;
    }

    public void setId_hab(byte id_hab) {
        this.id_hab = id_hab;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
